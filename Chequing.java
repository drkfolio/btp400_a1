package com.seneca.accounts;

import java.math.BigDecimal;

/**
 * 
 * 
 * 
 */

public class Chequing extends Account
{
    private BigDecimal serviceCharge;
    private int maxTransactions;
    private int numTransactions;
    private BigDecimal[] transactions;


    public Chequing() {this(null,null, 0.00,0.25,3);}

    public Chequing(String fullName, String accountNumber, double Balance, double serviceCharge, int maxTransactions)
    {
        super(fullName, accountNumber, Balance);
        setServiceCharge(serviceCharge);
        setMaxTransactions(maxTransactions);
    }

    public void setServiceCharge(double sCharge)
    {
        if (sCharge < 0.25)
        {
            serviceCharge = BigDecimal.valueOf(0.25);
        }
        else
        {
            serviceCharge = BigDecimal.valueOf(sCharge);
        }
    }

    public void setMaxTransactions(int mTransactions)
    {
        if (mTransactions < 3)
        {
            maxTransactions = 3;
        }
        else
        {
            maxTransactions = mTransactions;
        }
        transactions = new BigDecimal[maxTransactions];
    }

    public double getServiceCharge()
    {
        return serviceCharge.doubleValue();
    }
    
    public int getNumTransactions()
    {
    	return numTransactions;
    }
    
    public int getMaxTransactions()
    {
    	return maxTransactions;
    }

    @Override
    public boolean withdraw(double amount)
    {
        boolean didWork = false;

        if (numTransactions >= maxTransactions)
        {
            throw new ArrayIndexOutOfBoundsException("Attempting to exceed max number of Transactions");
        }

		if(amount < super.getBalance() && amount > 0 && numTransactions < maxTransactions)
		{
            numTransactions++;
            for (int i = 0; i < numTransactions; i++)
            {
                if (transactions[i] == null)
                {
                    transactions[i] = new BigDecimal(-1 * amount);
                }
            }
            didWork = super.withdraw(amount);
		}

		return didWork;
    }

    @Override
    public void deposit(double amount)
    {

        if (numTransactions == maxTransactions)
        {
            throw new ArrayIndexOutOfBoundsException("Attempting to exceed max number of Transactions");
        }

		if(amount > 0 && numTransactions < maxTransactions)
		{
            numTransactions++;
            for (int i = 0; i < numTransactions; i++)
            {
                if (transactions[i] == null)
                {
                    transactions[i] = new BigDecimal(amount);
                }
            }

            super.deposit(amount);
            
		}
    }

    @Override
    public double getBalance()
    {
        double fBalance = super.getBalance() - (serviceCharge.doubleValue() * numTransactions);
        return fBalance;
    }


    public String toString()
    {
        StringBuffer s = new StringBuffer();
        s.append(super.toString());

        s.append(
            "Account Type        : CHQ\n" +
            "Service Charge      : $" + getServiceCharge() + "\n" +
            "Total Charges       : $" + (getServiceCharge() * numTransactions) + "\n" +
            "List of Transactions: "
        );

        for (int i = 0; i < transactions.length; i++)
        {
            if (transactions[i].doubleValue() > 0)
            {
                s.append("+" + transactions[i]);
            }
            else
            {
                s.append(transactions[i]);
            }

            if(i + 1 < transactions.length)
            {
                s.append(",");
            }
            
        }

        s.append(
            "\nFinal Balance      : $" + getBalance());

        return s.toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;

        if(obj instanceof Chequing)
        {
            if (super.equals(obj))
            {
                Chequing testing = (Chequing) obj;

                if ((testing.serviceCharge.equals(serviceCharge)) && 
                (testing.maxTransactions == maxTransactions) &&
                (testing.numTransactions == numTransactions) &&
                (testing.transactions.equals(transactions)))
                {
                    result = true;
                }
            }

        }

        return result;
    }

    public int hashcode()
    {
        int hash = super.hashcode();

        hash = 31 * hash + serviceCharge.hashCode();
        hash = 31 * hash + maxTransactions;
        hash = 31 * hash + numTransactions;
        hash = 31 * hash + transactions.hashCode();

        return hash;
    }

}
