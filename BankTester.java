/**
       The purpose of this class is to hold main() and code
	   for creating/testing related to the class Bank

       @version 2.0
       @author  Derek Chan 27/01/2020

 */


class BankTester {

    public static void balanceCheck(Bank tBank, int val)
    {
        Account tmp[] = tBank.searchByBalance(val);
        if (tmp.length > 1)
        {
            System.out.println("We have found " + tmp.length + " accounts whose balance is $" + (int)tmp[0].getBalance() + ".");
            for (int i = 0 ; i < tmp.length; i++)
            {
                System.out.println(i+1 + ". number: " + tmp[i].getAccountNumber() + ", name: " + tmp[i].getFullName());
            }
        }
        else
        {
            System.out.println("*** NO ACCOUNT FOUND***");
        }
        System.out.println("\n");
    }

    public static void main(String[] args) 
    {

        String accountNames[] = { "John Doe", "Mary Ryan", "Peter Liu", "John Doe", "Peter Liu" };
        String accountNumbers[] = { "A1234", "B5678", "C9999", "A1234", "D8901" };
        int accountBalances[] = { 1000, 3000, 5000, 7000, 3000 };

        Bank testBank = new Bank("Derek Chan");
        System.out.println(testBank + "\n");


        System.out.println("Adding accounts to Bank of " + testBank.getBankName());
        for (int i = 0; i < accountNames.length; i++)
        {
            Account tmp = new Account(accountNames[i],accountNumbers[i],accountBalances[i]);
            testBank.addAccount(tmp);
        }
        System.out.println(testBank + "\n");


        System.out.println("Value of boolean when trying to add a null: " + testBank.addAccount(null) + "\n");

        System.out.println("Searching for Accounts with balance of $3000");
        balanceCheck(testBank, 3000);

        System.out.println("Searching for Accounts with balance of -$1111");
        balanceCheck(testBank, -1111);

        Bank testBank2 = new Bank("Derek Chan");
        for (int i = accountNames.length - 1; i > 0; i--)
        {
            Account tmp = new Account(accountNames[i],accountNumbers[i],accountBalances[i]);
            testBank2.addAccount(tmp);
        }
        System.out.println("Testing if testBank2 equals to testBank: " + testBank2.equals(testBank));


        Bank testBank3 = testBank;
        System.out.println("Testing if testBank3 equals to testBank: " + testBank3.equals(testBank));
    }

}