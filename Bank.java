import java.util.ArrayList;

/**
 * The purpose of this class is to carry the fields and methods that manage the
 * Bank class
 * 
 * @version 2.1
 * @author Derek Chan 27/01/2020
 *         Aten Cheung 20/02/2020
 * 
 */

public class Bank {
    private String bankName;
    private ArrayList<Account> accList;

    public Bank() {
        this(null);
    }

    public Bank(String banknm) {
        setBankName(banknm);
        accList = new ArrayList<Account>();
    }

    public void setBankName(String name) {
        if (name == null) {
            bankName = "Seneca@York";
        } else {
            bankName = name;
        }
    }

    public String getBankName() {
        return bankName;
    }

    public boolean addAccount(Account newAccount) {
        Boolean accAdded = false;

        if (newAccount != null) {
            Boolean dupeAcc = false;

            if (accList.size() == 0) {
                accList.add(newAccount);
                accAdded = true;
            } else {
                for (int i = 0; i < accList.size(); i++) {
                    if (newAccount.getAccountNumber().equals(accList.get(i).getAccountNumber())) {
                        dupeAcc = true;
                        break;
                    }
                }
                if (!dupeAcc) {
                    accList.add(newAccount);
                    accAdded = true;
                }
            }
        }
        return accAdded;
    }


    //MAY NEED TO CHANGE THE BALANCE PART HERE DEPENDING ON WHAT IS NEEDED LATER 
    //RIGHT NOW IS INT, MAY NEED TO BE DOUBLE OR BIGDECIMAL
    public String toString() {
        StringBuffer s = new StringBuffer();

        s.append(
                "*** Welcome to the Bank of " + getBankName() + " ***\n" + "It has " + accList.size() + " accounts.\n");

        for (int i = 0; i < accList.size(); i++) {
            int balance = (int) accList.get(i).getBalance();
            s.append(i + 1 + ". number: " + accList.get(i).getAccountNumber() + ", name: "
                    + accList.get(i).getFullName() + ", balance: $" + balance + "\n");
        }

        return s.toString();
    }

    public boolean equals(Object obj) {
        boolean result = false;

        if (obj instanceof Bank) {
            Bank testingBank = (Bank) obj;

            if ((testingBank.bankName.equals(bankName)) && (testingBank.accList.equals(accList))) {
                result = true;
            }
        }
        return result;
    }

    public Account[] searchByBalance(double bal) {
        Account tmp[] = new Account[0];
        int counter[] = new int[accList.size()];
        int j = 0;

        for (int i = 0; i < accList.size(); i++) {
            if (accList.get(i).getBalance() == bal) {
                counter[j] = i;
                j++;
            }
        }

        if (j > 0) {
            tmp = new Account[j];
            for (int k = 0; k < j; k++) {
                tmp[k] = accList.get(counter[k]);
            }
        }
        return tmp;
    }
/**
 * 
 * @param accountName
 * @return Account[] that contains Accounts that fit the accountName criteria 
 * @author Aten Cheung
 */
    public Account[] searchByAccountName(String accountName){
        Account tmp[] = new Account[0];
        int counter[] = new int[accList.size()];
        int j = 0;

        int commaIndex = accountName.indexOf(",");
        String accountNameLast = accountName.substring(0, commaIndex - 1);
        String accountNameFirst = accountName.substring(commaIndex + 2);

        for (int i = 0; i < accList.size(); i++) {
            if (accList.get(i).getLastName() == accountNameLast 
            && accList.get(i).getFirstName() == accountNameFirst) {
                counter[j] = i;
                j++;
            }
        }

        if (j > 0) {
            tmp = new Account[j];
            for (int k = 0; k < j; k++) {
                tmp[k] = accList.get(counter[k]);
            }
        }
        return tmp;
    }
/**
 * 
 * @return Account[] that contains all Accounts in the bank
 * @author Aten Cheung
 */
    public Account[] getAllAccounts(){
        Account tmp[] = new Account[accList.size()];
        for(int i = 0; i < accList.size(); i++){
            tmp[i] = accList.get(i);
        }
        return tmp;
    }
/**
 * remove account at specific index
 * @param index
 * @author Aten Cheung
 */
    public void removeAccount(int index){
        accList.remove(index);
    }

    public void depositIntoAccount(int index, double balance){
        accList.get(index).deposit(balance);
    }

    public void withdrawFromAccount(int index, double balance){
        accList.get(index).withdraw(balance);
    }
}