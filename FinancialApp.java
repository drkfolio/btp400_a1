package com.btp400;

import java.math.BigDecimal;
import java.util.*;

import com.seneca.business.Bank;
import com.seneca.accounts.*;

/**
 * @author Aten Cheung 2/23/2020
 * @version 1.0
 * 
 */

public class FinancialApp {
    public static void main(String[] args) {
        Bank tester = new Bank();
        Scanner scanObj = new Scanner(System.in);

        
        loadBank(tester);
        displayMenu(tester.getBankName());
        int userchoice = 0;
        do{
            userchoice = menuChoice(scanObj);
            switch(userchoice){
                case 1:
                    createAccount(tester,scanObj);
                    break;
                case 2:
                    deleteAccount(tester,scanObj);
                    break;
                case 3:
                    depositMoney(tester,scanObj);
                    break;
                case 4:
                    withdrawMoney(tester,scanObj);
                    break;
                case 5:
                    System.out.println(tester.toString());
                    break;
                case 6:
                    displayTaxStatement(tester, scanObj);
                    break;
                case 7:
                    System.out.println("Thank you for using our services.");
                    scanObj.close();
                    break;
                default:
                    System.out.println("Invalid input, please re-enter.");
            }
        } while(userchoice != 7);
        
    }


    /**
     * loading accounts into the bank param object. 2 GIC accounts and 2 chequing accounts.
     * @param bank
     */
    static void loadBank(Bank bank){
        Account gic1Account = new GIC("John Doe", "JD_G001", 6000.00, 2, 1.50);
        Account gic2Account = new GIC("Mary Ryan", "MR_G001", 15000.00, 4, 2.50);
        Account chequing1Account = new Chequing("John Doe", "JD_C001", 1000.00, 0.25, 3);
        Account chequing2Account = new Chequing("Mary Ryan", "MR_C001", 1000.00, 0.25, 3);
        bank.addAccount(gic1Account);
        bank.addAccount(gic2Account);
        bank.addAccount(chequing1Account);
        bank.addAccount(chequing2Account);
    }
    /**
     * Display menu with bankName param
     * @param bankName
     */
    static void displayMenu(String bankName){
        System.out.print("Welcome to " + bankName + " Bank!\n");
        System.out.println("1. Open an account.");
        System.out.println("2. Close an account.");
        System.out.println("3. Deposit money.");
        System.out.println("4. Withdraw money.");
        System.out.println("5. Display accounts.");
        System.out.println("6. Display a tax statement.");
        System.out.println("7. Exit");
    }
    /**
     * method for reading user choice for menu 
     * @return int object containing user choice
     */
    static int menuChoice(Scanner scanObj){
        System.out.println("Please enter your choice>");
        int userChoice = Integer.parseInt(scanObj.nextLine());
        return userChoice;
    }
    /**
     * Display the account in the param
     * @param account
     */
    static void displayAccount(Account account){
        System.out.println(account.toString());
    }

    static void createAccount(Bank bank, Scanner scanObj)
    {
        String Name;
        String accNum;
        double balance;

        double serviceCharge;
        int maxTransactions;

        int investmentPeriod;
        double interest;

        System.out.print("Please enter the account type (CHQ/GIC)> ");
        String accType = scanObj.nextLine();

        while(!accType.equals("CHQ") && !accType.equals("GIC"))
        {
                System.out.print("Please input a valid account type> ");
                accType = scanObj.nextLine();
        } 
            System.out.println("Please enter account information in one line\n");
            System.out.println("Example, John Doe;A1234;1000.00;1.5;2\n >");
            String accDetails = scanObj.nextLine();

        String [] wordInput = accDetails.split(";");
        for(int i = 0; i < wordInput.length; i++){
            wordInput[i].trim();
        }

        Name = wordInput[0];
        accNum = wordInput[1];
        balance = Double.parseDouble(wordInput[2]);

        if(accType == "CHQ")
        {
            serviceCharge = Double.parseDouble(wordInput[3]);
            maxTransactions = Integer.parseInt(wordInput[4]);
            bank.addAccount(new Chequing(Name,accNum,balance,serviceCharge,maxTransactions));
        }
        else
        {
            investmentPeriod = Integer.parseInt(wordInput[3]);
            interest = Double.parseDouble((wordInput[4]));
            bank.addAccount(new GIC(Name,accNum,balance,investmentPeriod,interest));
        }  
    }

    static int searchAccount(Bank bank, Scanner scanObj){


        System.out.println("Search account by name and balance (Doe, John;1000.00)> ");
        String [] userInput = scanObj.nextLine().split(";");
        for(int i = 0; i < userInput.length; i++){
            userInput[i].trim();
        }

        String name = userInput[0];
        double balance = Double.parseDouble(userInput[1]);

        Account[] nameSearchResults = bank.searchByAccountName(name);
        Account[] balanceSearchResults = bank.searchByBalance(balance);
        Account finalResult = new Account();
        int returnIndex = -1;

        for(int i = 0; i < nameSearchResults.length; i++){
            for(int k = 0; k < balanceSearchResults.length; k++){
                if(nameSearchResults[i].equals(balanceSearchResults[k])){
                    finalResult = nameSearchResults[i];
                }
            }
        }

        Account[] currentAccounts = bank.getAllAccounts();
        for(int i = 0; i < bank.getAllAccounts().length; i++){
            if(currentAccounts[i].equals(finalResult)){
                returnIndex = i;
            }
        }
        
        return returnIndex; //if not found will return -1
    }

    static void deleteAccount(Bank bank, Scanner scanObj){
        int userChoiceIndex = searchAccount(bank, scanObj);
        if(userChoiceIndex != -1){
            bank.removeAccount(userChoiceIndex);
        }
        else{
            System.out.println("---Account not found.---");
        }
    }

    static void depositMoney(Bank bank, Scanner scanObj){
        int userChoiceIndex = searchAccount(bank, scanObj);
        if(userChoiceIndex != -1){
            System.out.println("Please enter amount of money to deposit (1000.00)> ");
            String stringAmount = scanObj.nextLine();
            BigDecimal depositAmount = new BigDecimal(stringAmount);
            bank.depositIntoAccount(userChoiceIndex, depositAmount.doubleValue());

        }
        else{
            System.out.println("---Account not found.---");
        }
    }

    static void withdrawMoney(Bank bank, Scanner scanObj){
        int userChoiceIndex = searchAccount(bank, scanObj);
        if(userChoiceIndex != -1){
            System.out.println("Please enter amount of money to withdraw (1000.00)> ");
            String stringAmount = scanObj.nextLine();
            BigDecimal withdrawAmount = new BigDecimal(stringAmount);
            bank.withdrawFromAccount(userChoiceIndex, withdrawAmount.doubleValue());
        }
        else{
            System.out.println("---Account not found.---");
        }
    }

    static void displayTaxStatement(Bank bank, Scanner scanObj){
        int userChoiceIndex = searchAccount(bank, scanObj);
        if(userChoiceIndex != -1){
            Account[] allAccounts = bank.getAllAccounts();
            if(allAccounts[userChoiceIndex] instanceof GIC){
                System.out.println(((GIC)allAccounts[userChoiceIndex]).createTaxStatement());
            }
            else{
                System.out.println("---Account not a GIC account.---");
            }
        }
        else{
            System.out.println("---Account not found.---");
        }
    }
}