package com.seneca.accounts;
import java.math.BigDecimal;


/**
       The purpose of this class is to carry the fields and
	   methods that manage the Account class

       @version 2.0
       @author  Derek Chan 27/01/2020

 */

 /**
  * The purpose of this class is to carry the fields and
  * methods that manage the Account class
  * @version 3.0
  * @author  Derek Chan, Aten Cheung
  */

public class Account
{
	private String fullName;
	private String firstName;
	private String lastName;
	private String accountNumber;
	private BigDecimal accountBal;
	
	public Account() {this(null,null,0);}
	
	/**
	 * 	@param name		the full Name of the account holder
	 *	@param num		the account number
	 *	@param val		the balance for the above account
	 */

	public Account(String fullName, String accountNumber, double balance)
	{
		setFullName(fullName);
		setAccountNumber(accountNumber);
		setBalance(balance);
	}
	
	public void setFullName(String Name)
	{
		if (Name == null)
		{
			fullName = "";
			setFirstName(Name);
			setLastName(Name);
		}
		else
		{
			int spaceIndex = Name.indexOf(" ");
			if(spaceIndex != -1)
			{
				setFirstName(Name.substring(0, spaceIndex));
				setLastName(Name.substring(spaceIndex +1));
			}
			fullName = Name;
		}
	}

	public void setFirstName(String Name)
	{
		if (Name == null)
		{
			firstName = "";
		}
		else
		{
			firstName = Name;	
		}
		
	}

	public void setLastName(String Name)
	{
		if (Name == null)
		{
			lastName = "";
		}
		else
		{
			lastName = Name;			
		}
	}
	
	public void setAccountNumber(String num)
	{
		if (num == null)
		{
			accountNumber = "";
		}
		else
		{
			accountNumber = num;
		}

	}
	
	public void setBalance(double val)
	{
		if (val < 0)
		{
			accountBal = BigDecimal.valueOf(0.00);
		}
		else
		{
			accountBal = BigDecimal.valueOf(val);
		}

	}
	
	/**
	 *	@return fullName	is returning the Full name of the account holder of the current object
	 */
	public String getFullName()
	{
		return fullName;
	}

	/**
	 *	@return firstName	is returning the Full name of the account holder of the current object
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 *	@return lastName	is returning the Full name of the account holder of the current object
	 */
	public String getLastName()
	{
		return lastName;
	}
	
	/**
	 *	@return is returning the account number of the current object
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}
	

	/**
	 *	@return is returning the account balance of the current object
	 */
	public double getBalance()
	{
		return accountBal.doubleValue();
	}
	
	/**
	 * 
	 * @param amount is the amount of money attempting to be withdrawn
	 * @return is returning whether or not the withdrawal transaction was completed
	 */
	public boolean withdraw(double amount)
	{
		boolean didWork = false;

		if(amount < getBalance() && amount > 0)
		{
			setBalance(getBalance() - amount);
			didWork = true;
		}

		return didWork;
	}
	
	/**
	 * 
	 * @param amount is the amount of money attempting to be deposited
	 * @return is returning whether or not the deposit transaction was completed
	 */
	public void deposit(double amount)
	{
		if(amount > 0)
		{
			setBalance(getBalance() + amount);
		}
	}

	/**
	 *	@return is returning the formatted data when println is used on object of type Account
	 *	Need to do the padding of spaces for the Name and Number, doing ghetto way
	 */
	public String toString()
	{
		StringBuffer s = new StringBuffer();
		String balString = String.format("%.2f", getBalance());

		s.append(
			"Name           : " + getLastName() + ", " + getFirstName() + "\n" + 
			"Number         : " + getAccountNumber() + "\n" +
			"Current Balance: $" + balString + "\n");
		return s.toString();
	}

	/**
 	 *	@return returns true ONLY IF all three conditions are met
	 *		-the comparing Account names are exactly the same
	 *		-the comparing Account Numbers are exactly the same
	 *		-the comparing Balances are exactly the same 
	 */

	public boolean equals(Object obj)
	{
		boolean result = false;

		if (obj instanceof Account && obj != null)
		{
			Account testing = (Account) obj;

			BigDecimal accountbalTestA = new BigDecimal(getBalance());
			BigDecimal accountbalTestB = new BigDecimal(testing.getBalance());

			if ((testing.fullName.equals(fullName)) &&
				(testing.accountNumber.equals(accountNumber)) &&
				(accountbalTestB.equals(accountbalTestA)))
				{
					result = true;
				};
		}

		return result;
	}	

	public int hashcode()
	{
		int hash = 17;

		hash = hash * 31 + fullName.hashCode();
		hash = hash * 31 + accountNumber.hashCode();
		hash = hash * 31 + accountBal.hashCode();

		return hash;
	}

}