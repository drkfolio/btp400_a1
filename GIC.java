package com.seneca.accounts;

import java.math.BigDecimal;
import java.lang.Math;

/**
 * 
 * 
 * 
 * 
 */


 public class GIC 
    extends Account 
    implements Taxable
 {
    private BigDecimal taxAmount;
    private int investPeriod;
    private BigDecimal interestRate;

    public GIC() {this(null,null,0.00,1,1.25);}

    public GIC(String fullName, String accountNumber, double Balance, int investmentPeriod, double Interest)
    {
        super(fullName, accountNumber, Balance);
        setInvestmentPeriod(investmentPeriod);
        setInterestRate(Interest);
    }

    public void setInvestmentPeriod(int Years)
    {
        investPeriod = Years;
    }

    public void setInterestRate(double Interest)
    {
        interestRate = BigDecimal.valueOf(0.01 * Interest);
    }

    public int getInvestmentPeriod()
    {
        return investPeriod;
    }

    public double getInterestRate()
    {
        return interestRate.doubleValue();
    }

    public void calculateTax() {
        BigDecimal interestIncome = new BigDecimal(getBalance() - super.getBalance());
        taxAmount = interestIncome.multiply(taxRate);
    }

    public double getTaxAmount() {
        return taxAmount.doubleValue();
    }

    @Override
    public boolean withdraw(double amount)
    {
        return false;
    }

    @Override
    public void deposit(double amount) {}

    @Override
    public double getBalance() 
    {
        double balanceAtMaturity = super.getBalance() * Math.pow((new BigDecimal(1).add(interestRate).doubleValue()),(double)investPeriod);
        return balanceAtMaturity;
    }
    

    public String createTaxStatement() {

        StringBuffer s = new StringBuffer();

        s.append(super.toString());
        calculateTax();
        
        String interestIncomePrint = String.format("%6.2f", (getBalance()-super.getBalance()));
        String taxAmountPrint = String.format("%6.2f", taxAmount.doubleValue());

        s.append(
            "Tax rate       : " + (taxRate.doubleValue() * 100) + "%\n" +
            "Account Number : " + getAccountNumber() + "\n" +
            "Interest Income:  $" + interestIncomePrint + "\n" +
            "Amount of tax  : $" + taxAmountPrint + 
            "\n"
        );
        return s.toString();
    }

    public String toString()
    {
        StringBuffer s = new StringBuffer();

        s.append(super.toString());
        String interestIncomePrint = String.format("%6.2f", (getBalance()-super.getBalance()));
        String balanceString = String.format("%6.2f", getBalance());

        s.append(
            "Account Type               : GIC\n" +
            "Annual Interest Rate       : " + (getInterestRate() * 100) + "\n" +
            "Period of Investment       : " + getInvestmentPeriod() + " years\n" +
            "Interest Income at Maturity: $" + interestIncomePrint+ "\n" +
            "Balance at Maturity        : $" + balanceString
        );

        return s.toString();
    }

 }