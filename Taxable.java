package com.seneca.accounts;

import java.math.BigDecimal;

/**
 * Purpose of the Taxable.java file is to provide a interface for Taxable which
 * is used for the GIC class
 * 
 * @author Derek Chan
 */

interface Taxable 
{
static final BigDecimal taxRate = new BigDecimal(0.15);

public void calculateTax();
double getTaxAmount();
String createTaxStatement();

}