/**
       The purpose of this class is to hold main() and code
	   for creating/testing related to the class Account

       @version 2.0
       @author  Derek Chan 27/01/2020

 */

class AccountTest
{

	public static void compareAccounts(Account A, Account B)
	{
		if (A.equals(B))
		{
			System.out.println("These two are the same!" + "\n");
		}
		else
		{
			System.out.println("These two are not the same!" + "\n");
		}
	}

	public static void main(String[] args)
	{
		System.out.println("Creating Accounts for Peter, Lily and Derek\n\n");
		Account Peter, Derek1, Derek2, Lily;
		
		Peter = new Account("Peter Liu", "TD12345", 1000.99);
		Lily = new Account(null, null, -1);
		Derek1 = new Account("Derek Chan", "SB98765", 420.69);		
		Derek2 = new Account("Derek Chan", "SB98765", 420.69);

		
		System.out.println("Printing Peter's Details");
		System.out.println(Peter + "\n");
		
		System.out.println("Printing Lily's Details");
		System.out.println(Lily + "\n");
		
		System.out.println("Printing Derek's Details");
		System.out.println(Derek1 + "\n");
		
		System.out.println("Adding Lily's Details");
		Lily.setFullName("Lily Truong");
		Lily.setAccountNumber("RBC2468");
		Lily.setBalance(9000.90);
		
		System.out.println("Printing Lily's Details");
		System.out.println(Lily + "\n\n");

		System.out.println("Testing two Derek Accounts");
		compareAccounts(Derek1, Derek2);

		System.out.println("Testing if Derek Account equals Peter Account");
		compareAccounts(Derek1, Peter);

		System.out.println("Testing with one being NULL");
		compareAccounts(Derek1, null);

	}
}